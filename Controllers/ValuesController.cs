﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Data.SqlClient;
using Newtonsoft.Json;





namespace WEBAPISample.Controllers
{
    public class ValuesController : ApiController
    {
        SqlConnection con = new SqlConnection("server=192.168.1.15,1433;User ID=sa; Password=Pa$$w0rd; database=PMIS");

        public class Student
        {
            public int StudentID { get; set; }
            public string StudentName { get; set; }
        }


        // GET api/values
        public string Get()
        {
            //return new string[] { "value1", "value2" };
            SqlDataAdapter da = new SqlDataAdapter("select * From Student", con);
            DataTable dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();

            string output = "";

            if (dt.Rows.Count > 0)
            {
                output = JsonConvert.SerializeObject(dt);
                return output;
            }
            else
            {
                return "Data Not Found";
            }

        }

        // GET api/values/5
        public string Get(int id)
        {
            //return new string[] { "value1", "value2" };
            SqlDataAdapter da = new SqlDataAdapter("select * From Student Where StudentID = " + id, con);
            DataTable dt = new DataTable();
            con.Open();
            da.Fill(dt);
            con.Close();

            string output = "";

            if (dt.Rows.Count > 0)
            {
                output = JsonConvert.SerializeObject(dt);
                return output;
            }
            else
            {
                return "Data Not Found";
            }
        }

        // POST api/values
        public string Post([FromBody] Student student)
        {
            SqlCommand cmd = new SqlCommand("Insert Into Student(StudentID, StudentName) Values(" + student.StudentID + ",'" + student.StudentName + "')", con);
            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i== 1) { return "Saved Successfully " + student.StudentName; }
            else { return "Try again, Not Saved"; }
        }

        // PUT api/values/5
        public string Put(int id, [FromBody] string value)
        {
            SqlCommand cmd = new SqlCommand("Update Student Set StudentName = '" + value + "' Where StudentID = " + id, con);
            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i == 1) { return "Saved Successfully " + value; }
            else { return "Try again, Not Saved"; }
        }

        // DELETE api/values/5
        public string Delete(int id)
        {
            SqlCommand cmd = new SqlCommand("Delete From Student Where StudentID = " + id, con);
            con.Open();
            int i = cmd.ExecuteNonQuery();
            con.Close();

            if (i == 1) { return "Deleted Successfully " + id; }
            else { return "Try again, Not Deleted"; }
        }
    }
}
